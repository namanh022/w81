import React from 'react';
import './App.css';
import AddPresent from './components/addPresent/AddPresent';
import ListPresent from './components/listPresent/ListPresent';
import Header from './components/header/header'
import {BrowserRouter as Router,Routes, Route,Link} from "react-router-dom";
import { Button, Navbar, Nav ,Container } from 'react-bootstrap'
  
function App() {
  
  return (
    <div className="App">
      <div>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="#home">Portal</Navbar.Brand>
            <Nav className="me-auto">
            <Nav.Link href="https://portal.vamk.fi/mod/assign/view.php?id=585291">Home</Nav.Link>
            <Nav.Link href="https://gitlab.com/namanh022/w81">gitlab</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
      <Header name="Tran Anh - e1900305" />
        <Router>
          <div className="d-grid gap-2 mt-3">
          <Button variant="info" size="lg">
            <Link to="/add" >Add a present</Link></Button>
          <Button Link ="/list" variant="secondary" size="lg"><Link to="/list">list present</Link></Button>
        </div>
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Routes>
          <Route exact path="/add" element={<AddPresent/>}/>
          <Route exact path="/list" element={<ListPresent/>}/>
          
        </Routes>
      
        </Router>
        </div>
        </div>   

  );
}

export default App;
