import React from 'react';  //for gitlab CI test this need to be there even React 17 does not need it
import { configure, render, screen } from '@testing-library/react';
import App from './App';
import Adapter from "enzyme-adapter-react-16";
test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

configure({ adapter: new Adapter() });
describe("The text is correct", () => {
  it("Has one div", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find("div")).toHaveLength(1);
  })

  it("Has displays save to reloads", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find("div").text()).toEqual("Add a present");
  });
});