import React from 'react'
import { Button,Form,Row,Col } from 'react-bootstrap'
import './addPresent.css';
const AddPresent = (props) => {
  return (
    <div className='container'>
      
      <Form>
      
        <Row className="align-items-center">
        <Col xs="auto">
          <h2>Add Present :</h2>
          </Col>
          <Col xs="auto">
            <Form.Label htmlFor="inlineFormInput" visuallyHidden>Name</Form.Label>
            <Form.Control className="mb-2"id="present"placeholder="example: Jane Doe"/>
          </Col>

          <Col xs="auto">
            <Button type="submit" className="mb-2" onClick={save}>Submit</Button>
          </Col>
        </Row>
      </Form>

        
    </div>
  )
}

function save() {
    const name = document.getElementById("present").value;
    let present = {}
    present[name] = { name: name };
    alert(JSON.stringify(present))
    fetch("https://reactproject-final-default-rtdb.firebaseio.com/present.json", {
        method: "PATCH",
        body: JSON.stringify(present)
    });

}

export default AddPresent;