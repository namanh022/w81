import React,{useState, useEffect} from 'react'

function ListPresent(props) {
  const [presents, setPresents] = useState([]);
  
  useEffect(() => {
    fetch("https://reactproject-final-default-rtdb.firebaseio.com/present.json")
      .then(respone => respone.json())
      .then(data => {
        const presentArray = Array.from(Object.keys(data), k => data[k]);
        setPresents(presentArray);        
      })
  },[])
  const presentOut = presents.map((present, key) => {
    return <div> {present.name}</div>
 })
  
  return (
    <div>
      <h2>ListPresents</h2> 
    { presentOut }
    </div>
      )
}

export default ListPresent;